function addProduct(pId){
	$.get( "cart_actions.php?action=add&id="+pId, function( data ) {
	  location.reload();
	});
}

function removeProduct(pId){
	$.get( "cart_actions.php?action=remove&id="+pId, function( data ) {
	  location.reload();
	});
}

function setProductQuantity(pId, pQuantity){
	$.get( "cart_actions.php?action=set_quantity&id="+pId+"&quantity="+pQuantity, function( data ) {
	  location.reload();
	});
}

function addProductWithQuantity(pId){
	var quantity = $("#quantityToAddToCart").val();
	$.get( "cart_actions.php?action=add_w_quantity&id="+pId+"&quantity="+quantity, function( data ) {
	  location.reload();
	});
}