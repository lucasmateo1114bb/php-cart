<?php 

# Author Lucas Sánchez Madrid
# Creation Date : 26/06/2020

require_once("php_cart.php");
if(isset($_GET['action'])){
	if( $_GET['action']=="add"){
		add_product_to_cart($_GET['id']);
	}elseif($_GET['action']=="remove"){
		remove_product_from_cart($_GET['id']);
	}elseif($_GET['action']=="set_quantity"){
		set_product_quantity_from_cart($_GET['id'], $_GET['quantity']);
	}elseif($_GET['action']=="add_w_quantity"){
		add_product_to_cart_with_quantity($_GET['id'], $_GET['quantity']);
	}
}  
?>