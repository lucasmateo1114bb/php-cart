<?php

# Author Lucas Sánchez Madrid
# Creation Date : 26/06/2020

require_once("admin/includes/connection_admin.php");
#public $conn2 = $conn;
session_set_cookie_params(3600,"/");
session_start();

class Cart {
  public $mProducts;

  function __construct() {
    $this->mProducts = array();
  }

  function add_product_with_quantity($pId, $pQuantity) {
  	global $conn;
  	$isInProductList = false;
  	foreach ($this->mProducts as $product) {
  		if($product->get_id() == $pId){
  			$product->set_quantity(($product->get_quantity())+$pQuantity);
  			$isInProductList = true;
  			break;
  		}
  	}
  	if($isInProductList == false){
  		$product_query = "SELECT * FROM  producto WHERE id_producto= '".$pId."'";
  		$query_result = $conn->query($product_query);
  		$row_prod = mysqli_fetch_row($query_result);
  		$newProduct = new Product();
  		$newProduct->set_name($row_prod[1]);
  		$newProduct->set_price($row_prod[7]);
  		$newProduct->set_url_image("admin/img/upload/".$row_prod[10]);
  		$newProduct->set_id($row_prod[0]);
  		$newProduct->set_quantity($pQuantity);
  		array_push($this->mProducts, $newProduct);
  	}  	
  }

  function add_product($pId) {
  	global $conn;
  	$isInProductList = false;
  	foreach ($this->mProducts as $product) {
  		if($product->get_id() == $pId){
  			$product->set_quantity(($product->get_quantity())+1);
  			$isInProductList = true;
  			break;
  		}
  	}
  	if($isInProductList == false){
  		$product_query = "SELECT * FROM  producto WHERE id_producto= '".$pId."'";
  		$query_result = $conn->query($product_query);
  		$row_prod = mysqli_fetch_row($query_result);
  		$newProduct = new Product();
  		$newProduct->set_name($row_prod[1]);
  		$newProduct->set_price($row_prod[7]);
  		$newProduct->set_url_image("admin/img/upload/".$row_prod[10]);
  		$newProduct->set_id($row_prod[0]);
  		$newProduct->set_quantity(1);
  		array_push($this->mProducts, $newProduct);
  	}  	
  }

  function remove_product($pId) {
    foreach ($this->mProducts as $product) {
      if($product->get_id() == $pId){
        $key = array_search ($product, $this->mProducts);
        unset($this->mProducts[$key]);
        break;
      }
    }
  }

  function get_products() {
    return $this->mProducts;
  }

  function set_product_quantity($pId, $pQuantity) {
    if($pQuantity <= 0){
      $this->remove_product($pId);
    }else{
      foreach ($this->mProducts as $product) {
        if($product->get_id() == $pId){
          $product->set_quantity($pQuantity);
          break;
        }
      }
    }
    
  }

  function get_total(){
    $total = 0;
    foreach ($this->mProducts as $product) {
      $total += $product->get_price()*$product->get_quantity();
    }
    return $total;
  }
}

class Product {
  public $mName;
  public $mPrice;
  public $mId;
  public $mUrlImage;
  public $mQuantity;

  function get_subtotal(){
    return $this->mPrice*$this->mQuantity;
  } 

  function set_name($pName) {
    $this->mName = $pName;
  }

  function get_name() {
    return $this->mName;
  }

  function set_price($pPrice) {
    $this->mPrice = $pPrice;
  }

  function get_price() {
    return $this->mPrice;
  }

  function set_id($pId) {
    $this->mId = $pId;
  }

  function get_id() {
    return $this->mId;
  }

  function set_url_image($pUrlImage) {
    $this->mUrlImage = $pUrlImage;
  }

  function get_url_image() {
    return $this->mUrlImage;
  }

  function set_quantity($pQuantity) {
    $this->mQuantity = $pQuantity;
  }

  function get_quantity() {
    return $this->mQuantity;
  }

}

if (!isset($_SESSION["cart"])){
    $_SESSION["cart"] = new Cart();
}

function add_product_to_cart_with_quantity($pId, $pQuantity){
	$_SESSION["cart"]->add_product_with_quantity($pId, $pQuantity);
}

function add_product_to_cart($pId){
	$_SESSION["cart"]->add_product($pId);
}

function remove_product_from_cart($pId){
	$_SESSION["cart"]->remove_product($pId);
}

function get_products_from_cart(){
	return $_SESSION["cart"]->get_products();
}

function set_product_quantity_from_cart($pId, $pQuantity){
	$_SESSION["cart"]->set_product_quantity($pId, $pQuantity);
}

function get_total_from_cart(){
	return $_SESSION["cart"]->get_total();
}
?>